package com.astra.client.mapper;

import com.astra.client.database.domain.ClientEntity;
import com.astra.client.model.ClientResponse;
import com.astra.client.model.ClientRequest;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    ClientResponse mapEntityToDto(ClientEntity entity);

    List<ClientResponse> mapEntitiesToListDto(List<ClientEntity> entities);

    ClientEntity requestToClientEntity(ClientRequest request);

}
