package com.astra.client.utlis;

@FunctionalInterface
public interface CommonGetter<T> {

    T getValue();
}
