package com.astra.client.model.types;

import lombok.Getter;

import java.util.stream.Stream;

@Getter
public enum SourceNameOfService {
    MAIL,
    MOBILE,
    BANK,
    GOSUSLUGI;

    public static SourceNameOfService getByName(String source) {
        return Stream.of(values())
                .filter(v -> source.equalsIgnoreCase(v.name()))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
