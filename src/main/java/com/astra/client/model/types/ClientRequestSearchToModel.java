package com.astra.client.model.types;

import lombok.Getter;

@Getter
public enum ClientRequestSearchToModel {

    FIRST_NAME("firstName", "firstName"),
    LAST_NAME("lastName", "lastName"),
    MIDDLE_NAME("middleName", "middleName"),
    EMAIL("email", "email"),
    PHONE("phone", "lastName");

    private final String paramName;
    private final String fieldInDao;

    ClientRequestSearchToModel(String paramName, String fieldInDao) {
        this.paramName = paramName;
        this.fieldInDao = fieldInDao;
    }
}
