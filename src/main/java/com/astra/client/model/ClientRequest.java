package com.astra.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class ClientRequest {

    private Long bankId;

    private String lastName;

    private String firstName;

    private String middleName;

    private LocalDate birthDate;

    @Pattern(regexp = "\\d{4} \\d{6}")
    private String passportNumber;

    private String birthPlace;

    @Pattern(regexp = "7\\d{10}")
    private String phoneNumber;

    @Pattern(regexp = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$")
    private String email;

    private String registrationAddress;

    private String residenceAddress;
}
