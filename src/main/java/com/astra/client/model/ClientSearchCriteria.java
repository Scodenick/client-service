package com.astra.client.model;

import lombok.Data;

@Data
public class ClientSearchCriteria {

    private String lastName;
    private String firstName;
    private String middleName;
    private String phoneNumber;
    private String email;
}
