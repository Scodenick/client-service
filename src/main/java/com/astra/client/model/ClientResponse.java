package com.astra.client.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ClientResponse {

    private Long bankId;

    private String lastName;

    private String firstName;

    private String middleName;

    private LocalDate birthDate;

    private String passportNumber;

    private String birthPlace;

    private String phoneNumber;

    private String email;

    private String registrationAddress;

    private String residenceAddress;

}
