package com.astra.client.controller;

import com.astra.client.model.ClientRequest;
import com.astra.client.model.ClientResponse;
import com.astra.client.model.ClientSearchCriteria;
import com.astra.client.service.ClientService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/client-service")
public class ClientController {

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/{id}")
    public ClientResponse getClient(@PathVariable Long id) {
        return clientService.getClientById(id);
    }

    @GetMapping
    public List<ClientResponse> findClients(ClientSearchCriteria searchCriteria) {
        return clientService.getClientsByRequest(searchCriteria);
    }

    @PostMapping
    public ClientResponse createClient(
            @RequestHeader("x-source") String xSource,
            @RequestBody @Valid ClientRequest request) {
        return clientService.createClient(request, xSource);
    }

}
