package com.astra.client.database.spec;

import com.astra.client.database.domain.ClientEntity;
import com.astra.client.model.ClientSearchCriteria;
import com.astra.client.model.types.ClientRequestSearchToModel;
import com.astra.client.utlis.CommonGetter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import static com.astra.client.model.types.ClientRequestSearchToModel.*;

public class ClientSpecification implements Specification<ClientEntity> {

    private ClientSearchCriteria clientSearchCriteria;

    public ClientSpecification(ClientSearchCriteria clientSearchCriteria) {
        super();
        this.clientSearchCriteria = clientSearchCriteria;
    }

    @Override
    public Predicate toPredicate(Root<ClientEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        Predicate p = criteriaBuilder.conjunction();
        addParamInPredicate(p, root, FIRST_NAME, clientSearchCriteria::getFirstName);
        addParamInPredicate(p, root, LAST_NAME, clientSearchCriteria::getLastName);
        addParamInPredicate(p, root, MIDDLE_NAME, clientSearchCriteria::getMiddleName);
        addParamInPredicate(p, root, PHONE, clientSearchCriteria::getPhoneNumber);
        addParamInPredicate(p, root, EMAIL, clientSearchCriteria::getEmail);

        return p;
    }

    private void addParamInPredicate(Predicate p,
                                     Root<ClientEntity> root,
                                     ClientRequestSearchToModel model,
                                     CommonGetter<String> getter) {
        if(getter.getValue() != null) {
            p.getExpressions().add(root.get(model.getFieldInDao()).in(getter.getValue()));
        }
    }
}
