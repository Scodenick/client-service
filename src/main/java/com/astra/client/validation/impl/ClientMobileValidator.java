package com.astra.client.validation.impl;

import com.astra.client.model.ClientRequest;
import com.astra.client.model.types.SourceNameOfService;
import com.astra.client.validation.ClientValidator;
import org.springframework.stereotype.Component;

@Component
public class ClientMobileValidator implements ClientValidator {

    @Override
    public void validate(ClientRequest clientRequest) {
        if(clientRequest.getPhoneNumber() == null) {
            throw new IllegalArgumentException("Invalid request. Expected phone number");
        }
    }

    @Override
    public SourceNameOfService getType() {
        return SourceNameOfService.MOBILE;
    }

}
