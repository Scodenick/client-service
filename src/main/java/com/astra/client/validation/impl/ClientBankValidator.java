package com.astra.client.validation.impl;

import com.astra.client.model.ClientRequest;
import com.astra.client.model.types.SourceNameOfService;
import com.astra.client.validation.ClientValidator;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class ClientBankValidator implements ClientValidator {

    @Override
    public void validate(ClientRequest clientRequest) {
        if (ObjectUtils.anyNull(clientRequest.getBankId(),
                clientRequest.getBirthDate())
                ||
                StringUtils.isAnyEmpty(
                        clientRequest.getFirstName(),
                        clientRequest.getLastName(),
                        clientRequest.getMiddleName(),
                        clientRequest.getPassportNumber())) {
            throw new IllegalArgumentException("Invalid request. Expected: bankId, firstName, lastName, middleName, birthDate and passport number");
        }
    }

    @Override
    public SourceNameOfService getType() {
        return SourceNameOfService.BANK;
    }
}
