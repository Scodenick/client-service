package com.astra.client.validation.impl;

import com.astra.client.model.ClientRequest;
import com.astra.client.model.types.SourceNameOfService;
import com.astra.client.validation.ClientValidator;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class ClientGosuslugiValidator implements ClientValidator {

    @Override
    public void validate(ClientRequest clientRequest) {
        if (ObjectUtils.anyNull(
                clientRequest.getBankId(),
                clientRequest.getBirthDate(),
                clientRequest.getPhoneNumber())
                ||
                StringUtils.isAnyBlank(clientRequest.getFirstName(),
                        clientRequest.getLastName(),
                        clientRequest.getMiddleName(),
                        clientRequest.getRegistrationAddress(),
                        clientRequest.getPassportNumber())) {
            throw new IllegalArgumentException
                    ("Invalid request. Expected First name, last name, middle name, birth date, passport number, and phone number");
        }
    }

    @Override
    public SourceNameOfService getType() {
        return SourceNameOfService.GOSUSLUGI;
    }}
