package com.astra.client.validation.impl;

import com.astra.client.model.ClientRequest;
import com.astra.client.model.types.SourceNameOfService;
import com.astra.client.validation.ClientValidator;
import org.springframework.stereotype.Component;

@Component
public class ClientEmailValidator implements ClientValidator {

    @Override
    public void validate(ClientRequest clientRequest) {
        if(clientRequest.getEmail() == null || clientRequest.getFirstName() == null) {
            throw new IllegalArgumentException("Invalid request. Expected email and firstName");
        }
    }

    @Override
    public SourceNameOfService getType() {
        return SourceNameOfService.MAIL;
    }
}
