package com.astra.client.validation;

import com.astra.client.model.ClientRequest;
import com.astra.client.model.types.SourceNameOfService;

public interface ClientValidator {

    void validate(ClientRequest clientRequest);

    SourceNameOfService getType();
}
