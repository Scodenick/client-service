package com.astra.client.service;

import com.astra.client.model.ClientResponse;
import com.astra.client.model.ClientRequest;
import com.astra.client.model.ClientSearchCriteria;

import java.util.List;

public interface ClientService {

    List<ClientResponse> getClientsByRequest(ClientSearchCriteria request);

    ClientResponse getClientById(Long id);

    ClientResponse createClient(ClientRequest request, String source);
}
