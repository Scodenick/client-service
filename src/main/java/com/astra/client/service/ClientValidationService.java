package com.astra.client.service;

import com.astra.client.model.ClientRequest;

public interface ClientValidationService {

    void verify(ClientRequest request, String source);
}
