package com.astra.client.service.impl;

import com.astra.client.model.ClientRequest;
import com.astra.client.model.types.SourceNameOfService;
import com.astra.client.service.ClientValidationService;
import com.astra.client.validation.ClientValidator;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
@Getter
public class ClientValidationServiceImpl implements ClientValidationService {

    private final Map<SourceNameOfService, ClientValidator> validators;

    public ClientValidationServiceImpl(List<ClientValidator> validators) {
        this.validators = validators
                .stream()
                .collect(Collectors.toMap(
                        ClientValidator::getType,
                        Function.identity()
                ));
    }

    @Override
    public void verify(ClientRequest request, String source) {
        ClientValidator validator = Optional.ofNullable(validators.get(SourceNameOfService.getByName(source)))
                .orElseThrow(() -> new IllegalArgumentException("Invalid x-Source header value: " + source));
        validator.validate(request);
    }
}
