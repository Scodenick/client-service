package com.astra.client.service.impl;

import com.astra.client.database.dao.ClientRepository;
import com.astra.client.database.domain.ClientEntity;
import com.astra.client.database.spec.ClientSpecification;
import com.astra.client.mapper.ClientMapper;
import com.astra.client.model.ClientResponse;
import com.astra.client.model.ClientRequest;
import com.astra.client.model.ClientSearchCriteria;
import com.astra.client.service.ClientService;
import com.astra.client.service.ClientValidationService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;
    private final ClientValidationService clientValidationService;

    public ClientServiceImpl(ClientRepository clientRepository,
                             ClientMapper clientMapper,
                             ClientValidationService clientValidationService) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
        this.clientValidationService = clientValidationService;
    }

    @Override
    public List<ClientResponse> getClientsByRequest(ClientSearchCriteria request) {
        Specification<ClientEntity> spec = new ClientSpecification(request);
        List<ClientEntity> clientEntities = clientRepository.findAll(spec);
        return clientMapper.mapEntitiesToListDto(clientEntities);
    }

    @Override
    public ClientResponse getClientById(Long id) {
        ClientEntity clientEntity = clientRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
        return clientMapper.mapEntityToDto(clientEntity);
    }

    @Override
    @Transactional
    public ClientResponse createClient(ClientRequest request, String source) {
        clientValidationService.verify(request, source);
        ClientEntity clientEntity = clientRepository.save(clientMapper.requestToClientEntity(request));
        return clientMapper.mapEntityToDto(clientEntity);
    }
}
