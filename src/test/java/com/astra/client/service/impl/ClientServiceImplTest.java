package com.astra.client.service.impl;

import com.astra.client.database.dao.ClientRepository;
import com.astra.client.database.domain.ClientEntity;
import com.astra.client.mapper.ClientMapper;
import com.astra.client.model.ClientRequest;
import com.astra.client.model.ClientResponse;
import com.astra.client.model.ClientSearchCriteria;
import com.astra.client.service.ClientValidationService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class ClientServiceImplTest {

    @InjectMocks
    ClientServiceImpl  clientService;

    @Mock
    ClientRepository clientRepository;

    @Mock
    ClientValidationService clientValidationService;

    @Mock
    ClientMapper clientMapper;

    @Test
    void getClientsByRequest() {
        List<ClientEntity> clientEntities = Arrays.asList(new ClientEntity(), new ClientEntity());
        List<ClientResponse> expected = Arrays.asList(new ClientResponse(), new ClientResponse());
        ClientSearchCriteria criteria = new ClientSearchCriteria();

        when(clientRepository.findAll(Mockito.any(Specification.class))).thenReturn(clientEntities);
        when(clientMapper.mapEntitiesToListDto(clientEntities)).thenReturn(expected);

        List<ClientResponse> actual = clientService.getClientsByRequest(criteria);

        Assert.assertEquals(expected, actual);
        Mockito.verify(clientRepository, Mockito.times(1)).findAll(Mockito.any(Specification.class));
        Mockito.verify(clientMapper, Mockito.times(1)).mapEntitiesToListDto(clientEntities);

    }

    @Test
    void getClientById() {

        ClientEntity clientEntity = new ClientEntity();
        Long id = 1L;
        ClientResponse expected = new ClientResponse();

        Mockito.when(clientRepository.findById(id)).thenReturn(Optional.of(clientEntity));
        Mockito.when(clientMapper.mapEntityToDto(clientEntity)).thenReturn(expected);

        ClientResponse actual = clientService.getClientById(id);

        Assert.assertEquals(expected, actual);
        Mockito.verify(clientRepository, Mockito.times(1)).findById(id);
        Mockito.verify(clientMapper, Mockito.times(1)).mapEntityToDto(clientEntity);
    }

    @Test
    void createClient() {
        ClientRequest request = new ClientRequest();
        String source = "test";
        ClientEntity clientEntity = new ClientEntity();
        ClientResponse expected = new ClientResponse();

        Mockito.doNothing().when(clientValidationService).verify(request, source);

        // Конфигурация мока репозитория
        Mockito.when(clientRepository.save(Mockito.any(ClientEntity.class))).thenReturn(clientEntity);

        // Конфигурация мока маппера
        Mockito.when(clientMapper.mapEntityToDto(clientEntity)).thenReturn(expected);
        Mockito.when(clientMapper.requestToClientEntity(request)).thenReturn(clientEntity);

        ClientResponse actual = clientService.createClient(request, source);

        Assert.assertEquals(expected, actual);
        Mockito.verify(clientValidationService, Mockito.times(1)).verify(request, source);
        Mockito.verify(clientRepository, Mockito.times(1)).save(clientEntity);
        Mockito.verify(clientMapper, Mockito.times(1)).mapEntityToDto(clientEntity);
        Mockito.verify(clientMapper, Mockito.times(1)).requestToClientEntity(request);
    }
}