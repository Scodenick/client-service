package com.astra.client.validation.impl;

import com.astra.client.model.ClientRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ClientEmailValidatorTest {

    @InjectMocks
    ClientEmailValidator validator;

    @Test
    void validate() {

        ClientRequest request1 = new ClientRequest()
                .setFirstName("John")
                .setLastName("Doe")
                .setEmail("john@example.com");
        assertDoesNotThrow(() -> validator.validate(request1));

        ClientRequest request2 = new ClientRequest()
                .setFirstName("John")
                .setLastName("Doe")
                .setEmail(null);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request2));
        assertEquals("Invalid request. Expected email and firstName", exception.getMessage());

        ClientRequest request3 = new ClientRequest()
                .setFirstName(null)
                .setLastName("Doe")
                .setEmail("john@example.com");
        exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request3));
        assertEquals("Invalid request. Expected email and firstName", exception.getMessage());

        ClientRequest request4 = new ClientRequest()
                .setFirstName(null)
                .setLastName("Doe")
                .setEmail(null);
        exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request4));
        assertEquals("Invalid request. Expected email and firstName", exception.getMessage());
    }
}