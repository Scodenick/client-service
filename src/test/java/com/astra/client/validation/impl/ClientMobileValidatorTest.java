package com.astra.client.validation.impl;

import com.astra.client.model.ClientRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ClientMobileValidatorTest {

    @InjectMocks
    ClientMobileValidator validator;

    @Test
    void validate() {
        ClientRequest request = new ClientRequest()
                .setFirstName("John")
                .setLastName("Doe")
                .setPhoneNumber("70934839402");
        assertDoesNotThrow(() -> validator.validate(request));
    }

    @Test
    void validateWithoutPhone() {
        ClientRequest request = new ClientRequest()
                .setFirstName("John")
                .setLastName("Doe");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request));
        assertEquals("Invalid request. Expected phone number", exception.getMessage());
    }
}