package com.astra.client.validation.impl;

import com.astra.client.model.ClientRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ClientBankValidatorImplTest {

    @InjectMocks
    private ClientBankValidator clientBankValidator;

    @Test
    public void validate() {
        ClientRequest clientRequest = new ClientRequest();
        clientRequest.setBankId(111L);
        clientRequest.setFirstName("John");
        clientRequest.setLastName("Doe");
        clientRequest.setMiddleName("Smith");
        clientRequest.setBirthDate(LocalDate.of(1990, 1, 1));
        clientRequest.setPassportNumber("1234567890");

        assertDoesNotThrow(() -> clientBankValidator.validate(clientRequest));
    }

    @Test
    public void testEmptyBankId() {
        ClientRequest clientRequest = new ClientRequest();
        clientRequest.setFirstName("John");
        clientRequest.setLastName("Doe");
        clientRequest.setMiddleName("Smith");
        clientRequest.setBirthDate(LocalDate.of(1990, 1, 1));
        clientRequest.setPassportNumber("1234567890");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> clientBankValidator.validate(clientRequest));
        assertEquals("Invalid request. Expected: bankId, firstName, lastName, middleName, birthDate and passport number", exception.getMessage());
    }

    @Test
    public void testEmptyFirstName() {
        ClientRequest clientRequest = new ClientRequest();
        clientRequest.setBankId(1L);
        clientRequest.setLastName("Doe");
        clientRequest.setMiddleName("Smith");
        clientRequest.setBirthDate(LocalDate.of(1990, 1, 1));
        clientRequest.setPassportNumber("1234567890");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> clientBankValidator.validate(clientRequest));
        assertEquals("Invalid request. Expected: bankId, firstName, lastName, middleName, birthDate and passport number", exception.getMessage());
    }

    @Test
    public void testEmptyLastName() {
        ClientRequest clientRequest = new ClientRequest();
        clientRequest.setBankId(17L);
        clientRequest.setFirstName("John");
        clientRequest.setMiddleName("Smith");
        clientRequest.setBirthDate(LocalDate.of(1990, 1, 1));
        clientRequest.setPassportNumber("1234567890");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> clientBankValidator.validate(clientRequest));
        assertEquals("Invalid request. Expected: bankId, firstName, lastName, middleName, birthDate and passport number", exception.getMessage());
    }

    @Test
    public void testEmptyMiddleName() {
        ClientRequest clientRequest = new ClientRequest();
        clientRequest.setBankId(13L);
        clientRequest.setFirstName("John");
        clientRequest.setLastName("Doe");
        clientRequest.setBirthDate(LocalDate.of(1990, 1, 1));
        clientRequest.setPassportNumber("1234567890");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> clientBankValidator.validate(clientRequest));
        assertEquals("Invalid request. Expected: bankId, firstName, lastName, middleName, birthDate and passport number", exception.getMessage());
    }

    @Test
    public void testEmptyBirthDate() {
        ClientRequest clientRequest = new ClientRequest();
        clientRequest.setBankId(12L);
        clientRequest.setFirstName("John");
        clientRequest.setLastName("Doe");
        clientRequest.setMiddleName("Smith");
        clientRequest.setPassportNumber("1234567890");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> clientBankValidator.validate(clientRequest));
        assertEquals("Invalid request. Expected: bankId, firstName, lastName, middleName, birthDate and passport number", exception.getMessage());
    }

    @Test
    public void testEmptyPassportNumber() {
        ClientRequest clientRequest = new ClientRequest();
        clientRequest.setBankId(11L);
        clientRequest.setFirstName("John");
        clientRequest.setLastName("Doe");
        clientRequest.setMiddleName("Smith");
        clientRequest.setBirthDate(LocalDate.of(1990, 1, 1));

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> clientBankValidator.validate(clientRequest));
        assertEquals("Invalid request. Expected: bankId, firstName, lastName, middleName, birthDate and passport number", exception.getMessage());
    }
}