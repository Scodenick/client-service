package com.astra.client.validation.impl;

import com.astra.client.model.ClientRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class ClientGosuslugiValidatorTest {

    @InjectMocks
    ClientGosuslugiValidator validator;

    @Test
    void validate() {
        ClientRequest request = getRequest();
        assertDoesNotThrow(() -> validator.validate(request));
    }

    @Test
    public void testValidateEmptyFirstName() {
        ClientRequest request = getRequest();
        request.setFirstName("");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request));
    }

    @Test
    public void testValidateNullBankId() {
        ClientRequest request = getRequest();
        request.setBankId(null);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request));
    }

    @Test
    public void testValidateEmptyLastName() {
        ClientRequest request = getRequest();
        request.setLastName(null);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request));
    }

    @Test
    public void testValidateEmptyMiddleName() {
        ClientRequest request = getRequest();
        request.setMiddleName(null);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request));
    }

    @Test
    public void testValidateEmptyRegistrationAddress() {
        ClientRequest request = getRequest();
        request.setRegistrationAddress(null);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request));
    }

    @Test
    public void testValidateEmptyPassportNumber() {
        ClientRequest request = getRequest();
        request.setPassportNumber(null);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request));
    }

    @Test
    public void testValidateNullBirthDate() {
        ClientRequest request = getRequest();
        request.setBirthDate(null);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request));
    }

    @Test
    public void testValidateNullPhoneNumber() {
        ClientRequest request = getRequest();
        request.setPhoneNumber(null);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> validator.validate(request));
    }

    private ClientRequest getRequest() {
        return new ClientRequest()
                .setFirstName("John")
                .setLastName("Doe")
                .setMiddleName("Smith")
                .setRegistrationAddress("Some address")
                .setPassportNumber("123456789")
                .setBirthDate(LocalDate.now())
                .setPhoneNumber("1234567890")
                .setBankId(11L);
    }
}