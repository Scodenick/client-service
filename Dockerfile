FROM bellsoft/liberica-openjdk-alpine-musl
COPY ./target/client-service-0.0.1-SNAPSHOT.jar .
CMD ["java","-jar","client-service-0.0.1-SNAPSHOT.jar"]